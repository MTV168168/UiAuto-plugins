const _ = require("lodash");
var util = require("./util.js");
const fs = require("fs");

/**
 * 模板方法
 */
exports.start = function (params) {
  console.log('lalalallala=-=-=-=-=-=lalallalallala', params);
  const promise = new Promise((resolve, reject) => {
    resolve(true);
  });

  return promise;
};


exports.condition = function (params, global_data_store) {
  var reg = /(?<!=)=(?!=)/;
  console.log('-------condition--------');
  console.log(params);
  console.log(global_data_store);

  const promise = new Promise((resolve, reject) => {

    const keys = _.keys(global_data_store);
    const values = _.values(global_data_store);
    const execute_results = [];

    _.forEach(params, (condition) => {
      const value_match = condition.expression.match(/\${(.*?)}/g);
      console.log("value_match>>>>>>>>>", value_match);
      if (!!value_match && value_match.length > 0) {
        let temp_value = condition.expression;
        if (value_match.length === 1) {
          const value = value_match[0];
          const field = value.replace(/\${|}/g, "");
          const fun = new Function(keys, "return typeof " + field + " !== 'undefined' ? " + field + " : null");
          temp_value = fun(...values);
        } else {
          _.forEach(value_match, (value) => {
            const field = value.replace(/\${|}/g, "");
            const fun = new Function(keys, "return typeof " + field + " !== 'undefined' ? " + field + " : null");
            const field_value = fun(...values);
            temp_value = temp_value.replace(eval("/" + value + "/g"), JSON.stringify(field_value));
          })
        }

        execute_results.push({
          name: condition.name,
          expression: condition.expression,
          status: !!temp_value
        });
      } else {
        execute_results.push({
          name: condition.name,
          expression: condition.expression,
          status: !!condition.expression
        });
      }
    });

    // return result
    resolve(execute_results);
  });

  return promise;
};

exports.circulation = function (params) {
  const promise = new Promise((resolve, reject) => {
    resolve(params);
  });

  return promise;
};


exports.end = function (params) {
  const promise = new Promise((resolve, reject) => {
    console.log(params);
    resolve(true);
  });

  return promise;
};


String.prototype.format = function () {
  const locate_reg = /\${(.*?)}/g;
  if (arguments.length == 0) return this;

  var param = arguments[0];
  var s = this;

  var replace_areas = _.uniq(s.match(locate_reg));
  _.each(replace_areas, area => {
    let target_argument = area.substring(2, area.length-1);
    console.log(area)
    console.log(target_argument);
    let target_string = eval(`param.${target_argument}`);
    typeof target_string === 'string' && (target_string = `\\"${target_string}\\"`);

    console.log(target_string);
    s = s.replace(new RegExp(area.replace(/\$/g, '\\$'), 'g'), target_string);
  })
  return s;
}
