# -*- coding: utf-8 -*-
import os
import time
import json


def playSound(params):
    try:
        import pygame
        from pymediainfo import MediaInfo
        import shutil

        path = params["path"]
        dllPath = params['uiauto_config']["client_dir"] + "\\env\\python\\win32\\MediaInfo.dll"
        if not os.path.isfile(path):
            raise Exception("不是文件")
        if not os.path.exists(path):
            raise Exception("文件不存在")
        if not os.path.exists(dllPath):
            shutil.copy(params['uiauto_config']['plugins_dir']+r"\system\MediaInfo.dll", params['uiauto_config']["client_dir"] + r"\\env\\python\\win32")

        pygame.mixer.init()
        pygame.mixer.music.load(path)

        music = MediaInfo.parse(path).to_json()
        music = json.loads(music)
        length = music['tracks'][0]['duration'] / 1000

        while True:
            pygame.mixer.music.play(loops=0, start=0.0)
            pygame.mixer.music.set_volume(0.5)
            time.sleep(length)
            pygame.mixer.music.stop()
            break
    except Exception:
        raise Exception("播放声音出错")


def fileIsExist(params):
    path = params['path']
    data = False
    try:
        if os.path.exists(path):
            data = True
    except Exception:
        raise Exception('判断出错')
    return data


def getEnv(params):
    try:
        env = params['env']
        if env == '':
            return dict(os.environ)
        else:
            return os.environ[env]
    except KeyError:
        return "没有该环境变量"
    except Exception as e:
        raise Exception("获取环境变量出错")


def setEnv(params):
    try:
        env = params["env"]
        value = params["value"]
        os.environ[env] = value
    except Exception:
        raise Exception("设置环境变量出错")


def execCommand(params):
    try:
        cmd = params["cmd"]
        result = os.popen(cmd, 'r')
        return result.read()
    except Exception:
        raise Exception("执行命令行出错")


def execPowerShell(params):
    try:
        cmd = params["cmd"]
        PowerShellCommand = 'powershell -Command "' + str(cmd) + '"'
        output = os.popen(PowerShellCommand, 'r')
        return output.read()
    except Exception:
        raise Exception("执行PowerShell出错")


def getSystemPath(params):
    try:
        import win32api
        path = params['type']
        if (path == "desktop"):
            return os.path.join(GetUserPath(params), "Desktop")
        elif (path == "start"):
            import win32com.client
            objShell = win32com.client.Dispatch("WScript.Shell")
            return objShell.SpecialFolders("StartMenu")
        elif (path == "install"):
            return os.environ['programfiles']
        elif (path == "windows"):
            return win32api.GetWindowsDirectory()
        elif (path == "system"):
            return win32api.GetSystemDirectory()
    except Exception:
        raise Exception("获取系统目录出错")


def getTempPath(params):
    import tempfile
    return tempfile.gettempdir()


def GetUserPath(params):
    from os.path import expanduser
    return expanduser("~")


if __name__ == "__main__":
    pass
