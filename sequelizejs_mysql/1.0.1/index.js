const path = require('path');
const Sequelize = require('sequelize');

var connectMysql = (params) => {
  const promise = new Promise((resolve, reject) => {
    connect(params)
      .then(res => {
        resolve(params)
      }).catch(err => {
        reject(err)
      })
  })

  return promise;
}
exports.connectMysql = connectMysql;

var connect = function (connect_message) {
  return new Promise((resolve, reject) => {
    var db = {
      Sequelize,
      sequelize: new Sequelize(connect_message.database_name, connect_message.username, connect_message.password, {
        host: connect_message.host,
        port: connect_message.port,
        dialect: 'mysql',
        logging: function () { },
      })
    };

    db.sequelize.sync({
      force: false
    })
      .then(function () {
        resolve(db);
      })
  })
}

exports.executeSql = params => {
  return connect(params.connect_message).then(db => {
    return db.sequelize.query(params.sql).then(res => {
      return res[0]
    })
  })
}