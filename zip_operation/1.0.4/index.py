import os
import io
import sys
import base_zip.zipfile as zipfile
#from base_zip import zipfile
#import base_zip


"""
TODO:
    - 批量解压zip文件
"""
def batchCompressZip(params):
    try:
        result_tips = ''
        target_file_dir = params['target_file_dir']
        output_dir = params['output_dir']
        is_create_dir = params['is_create_dir']

        file_names = os.listdir(target_file_dir)
        for file_name in file_names:
            zip_file_path = '%s\\%s' % (target_file_dir, file_name)
            ext = (os.path.splitext(file_name)[1] if os.path.isfile(zip_file_path) else None)
            if ext is not None and ext == '.zip':
                decompressZip({
                    'zip_file_path': zip_file_path,
                    'output_path': output_dir,
                    'is_create_dir': is_create_dir
                })
        result_tips = '解压完成'
        return result_tips
    except Exception as e:
        raise e


"""
TODO:
    - 解压单个zip文件
"""
def decompressZip(params):
    try:
        zip_file_path = params['zip_file_path']
        output_path = params['output_path']
        is_create_dir = params['is_create_dir']
    
        if not os.path.isfile(zip_file_path):
            raise Exception('源文件不是一个文件')
        z_file_name = os.path.basename(zip_file_path)
        if os.path.splitext(z_file_name)[1] != '.zip':
            raise Exception('源文件类型尚未支持')
        if is_create_dir is True:
            output_path = u'%s\\%s' % (output_path, os.path.splitext(z_file_name)[0])
        z_file = zipfile.ZipFile(zip_file_path, 'r')
        for fileM in z_file.namelist():
            z_file.extract(fileM, output_path)
        z_file.close()
        print('解压完成')
    except Exception as e:
        print(e)
        raise e


"""
TODO:
    - 将指定目录压缩成zip文件
"""
def compressZip(params):
    result_tips = ''
    try:
        target_file_path = params['target_file_path']
        output_path = params['output_path']
        zip_file_name = params['zip_file_name']
        z_file = zipfile.ZipFile('%s\\%s.zip' % (output_path, zip_file_name), 'w')
        file_names = os.listdir(target_file_path)
        for file_name in file_names:
            z_file.write('%s\\%s' % (target_file_path, file_name), file_name, zipfile.ZIP_DEFLATED)
        z_file.close()
        result_tips = '压缩成功'
    except PermissionError:
        result_tips = '没有文件读写权限'
    except Exception as e:
        result_tips = '压缩出错'
        raise e
    return result_tips


if __name__ == "__main__":

    # batchCompressZip({
    #     'target_file_dir': 'Z:\\workspace\\legion\\code\\uiauto-plugins\\zip\\',
    #     'output_dir': 'Z:\\workspace\\legion\\code\\uiauto-plugins\\zip\\temp',
    #     'is_create_dir': True
    # })

    decompressZip({
        'zip_file_path': 'C:\\普惠信贷系统对账.zip',
        'output_path': 'C:\\Temp',
        'is_create_dir': True
    })

    # compressZip({
    #     'target_file_path': 'Z:\\workspace\\legion\\code\\uiauto-plugins\\zip\\test',
    #     'output_path': 'Z:\\workspace\\legion\\code\\uiauto-plugins\\zip\\temp',
    #     'zip_file_name': 'test'
    # })