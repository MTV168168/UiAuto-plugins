import datetime
import time
import base64
import urllib3
import traceback
import os
import json
from PIL import Image, ImageDraw, ImageFont
from requests.sessions import Session
from fake_useragent import UserAgent
from pprint import pprint

# from js_function import getSwjg, get_fplx

ua = UserAgent()
urllib3.disable_warnings()


class CaptchaError(Exception):
    pass


class ConnectError(Exception):
    pass


def execute_js(js):
    sess = Session()
    data = {
        'js': js,
    }
    res = sess.post(url='http://119.3.27.15:6000/api/invoice/executeJs', data=data)
    # print(res.json())
    return res.json()['result']


def getSwjg(fpdm):
    return execute_js(f"""
        return getSwjg('{fpdm}')
    """)


def get_fplx(fpdm):
    return execute_js(f"""
        return alxd('{fpdm}')
    """)


def get_test(image, color):
    sess = Session()
    url = 'http://152.136.207.29:19812/captcha/v1'
    if '请输入验证码文字' in color:
        data = {"image": image}
    else:
        data = {"image": image, "param_key": color}

    return sess.post(url, json=data).json()['message']


def get_yzm(path, color, username, password):
    img = Image.open(path)
    back_img = Image.new('RGBA', (150, 70), 'white')
    back_img.paste(img, (0, 35))
    draw = ImageDraw.Draw(back_img)
    font = ImageFont.truetype("C:\\Windows\\Fonts\\simsun.ttc", 20)  # 现在是宋体
    draw.text((0, 0), f"请输入{color}文字", (0, 0, 0), font=font)
    back_img.save(path)
    yzm = ocr_verify_code(path, yzm_type='1103', username=username, password=password)
    return yzm


def ocr_verify_code(file_path, yzm_type='', try_times=5, username='', password=''):
    """
    验证码识别
    :param file_path: 待识别的图片路径（绝对路径）
    :param yzm_type：验证码类型（默认为空）
                    具体类型可参考：https://www.jsdati.com/docs/price
    :param api_url: API接口(默认为空)
    :param username: 账户(默认为空)
    :param password: 密码(默认为空)
    :return:
    """

    try:
        # 要上传到打码平台的数据
        api_post_url = "https://v1-http-api.jsdama.com/api.php?mod=php&act=upload"
        # api_post_url = api_url
        yzm_min = ''
        yzm_max = ''
        tools_token = ''
        data = {"user_name": username,
                "user_pw": password,
                "yzm_minlen": yzm_min,
                "yzm_maxlen": yzm_max,
                "yzmtype_mark": yzm_type,
                "zztool_token": tools_token,
                }
        files = {
            'upload': (os.path.basename(file_path), open(file_path, 'rb'), 'image/png')
        }
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language': 'zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',
            'Connection': 'keep-alive',
            'Host': 'v1-http-api.jsdama.com',
            'Upgrade-Insecure-Requests': '1'
        }
        # 实例化连接对象
        s = Session()
        result = s.post(api_post_url, headers=headers, data=data, files=files, verify=False, timeout=30)
        # 返回数据
        result = result.text
        res = json.loads(result)
        if res['result']:
            return res['data']['val']
        else:
            return None
    except Exception as err:
        if try_times > 0:
            return ocr_verify_code(file_path, yzm_type, try_times - 1, username=username, password=password)
        else:
            return 'OCR Error: {}'.format(err)


def input_check(fpdm, fphm, kprq, jym, fpje):
    if len(fpdm) not in [10, 12]:
        return '发票代码输入格式错误'
    elif len(fphm) != 8:
        return '发票号码输入格式错误'
    elif len(fpdm) == 10 and not fpje:
        return '请输入金额'
    elif len(fpdm) == 12 and len(jym) != 6:
        return '校验码格式有误'
    elif len(kprq) != 8:
        return '日期格式错误'
    return True


def check(fpdm, fphm, kprq, jym='', fpje='', username='', password=''):
    s = time.time()
    check_res = input_check(fpdm, fphm, kprq, jym, fpje)

    # 校验类型
    if check_res is not True:
        return json.dumps({'校验状态': False, '真伪': False, '结果信息': '%s' % check_res}, ensure_ascii=False)
    province = getSwjg(fpdm)
    if len(province) == 0:
        return json.dumps({'校验状态': False, '真伪': False, '结果信息': '输入格式错误'}, ensure_ascii=False)

    sess = Session()
    headers = {
        'User-Agent': ua.random
    }
    yzm_url = province[1] + '/yzmQuery'
    check_url = f"{province[1].split(':443')[0]}/NWebQuery/vatQuery"
    js = """
        var d = {};
        nowtime = showTime().toString();
        d['nowtime'] = nowtime;
        d['r'] = Math['random']();
        d['publickey'] = $['psiss']['yzmqSign']('%s', '%s', nowtime);
        d['key9'] = $[_0x419b('0x10')][_0x419b('0x11')]('%s', '%s', nowtime);
        return d
    """ % (fpdm, fphm, fpdm, fphm)
    # print(js)
    d = execute_js(js)
    # print(d, '2222')
    data = {
        'fpdm': fpdm,
        'fphm': fphm,
        'r': d['r'],
        'v': 'V2.0.04_004',
        'nowtime': d['nowtime'],
        'publickey': d['publickey'],
        'key9': d['key9'],
    }
    res = sess.post(yzm_url, params=data, headers=headers, verify=False)
    # 有两个查询类型。第一种是需要校验码不需要开票金额的，第二种是只需要开票金额不需要校验码，js没找到在哪判断的，可以根据验证码返回值判断
    # 返回值有data说明需要解码。也就是需要校验码的，不含有data含有key1的说明是解码好的值，查询时不需要校验吗需要开票金额
    if 'data' in res.json().keys():
        print('此次是校验码查询')
        query_type = 1
        js_decode = f"""
             a = replaceStr('{res.json()['data']}', '{d['nowtime']}');
             a = Base64['decode'](a);
             a = eval('(' + a + ')');
             return a
        """
        decode_data = execute_js(js_decode)
    else:
        query_type = 2
        print('此次是开票金额查询')
        decode_data = res.json()
    # print('decode_data:', decode_data)
    # time.sleep(111)
    img_data = base64.b64decode(decode_data['key1'])
    with open('yzm.png', 'wb') as f:
        f.write(img_data)
    # color_map = {'00': '请输入验证码文字', '01': 'red', '02': 'yellow', '03': 'blue'}
    color_map = {'00': '全部', '01': '红色', '02': '黄色', '03': '蓝色'}
    print('请输入 %s 文字' % color_map[decode_data['key4']])
    s_yzm = time.time()
    yzm = get_yzm('yzm.png', color_map[decode_data['key4']], username=username, password=password)
    print(f'验证码识别成功:{yzm}，耗时:{time.time() - s_yzm}')
    js = """
        var d = {};
        d['fplx'] = alxd('%s');
        // d['publickey'] = $[_0x163a('0x8b')][_0x163a('0x8c')]('%s', '%s', '%s');
        d['key9'] = $[_0x2d58('0x13')]['cy']('%s', '%s', '%s');
        return d
    """ % (fpdm, fpdm, fphm, decode_data['key2'], fpdm, fphm, decode_data['key2'])
    # print(js)
    ss = time.time()
    d2 = execute_js(js)
    print(time.time() - ss, 'exe')
    # return
    if query_type == 1 or d2['fplx'] in ['11']:
        data2 = {
            'key1': fpdm,
            'key2': fphm,
            'key3': kprq,
            'key4': jym,
            'fplx': d2['fplx'],
            'yzm': yzm,
            'yzmSj': decode_data['key2'],
            'index': decode_data['key3'],
            'publickey': d['publickey'],
            'key9': d2['key9'],
        }
    else:
        data2 = {
            'key1': fpdm,
            'key2': fphm,
            'key3': kprq,
            'key4': fpje,
            'fplx': d2['fplx'],
            'yzm': yzm,
            'yzmSj': decode_data['key2'],
            'index': decode_data['key3'],
            'publickey': d['publickey'],
            'key9': d2['key9'],
        }
    res2 = sess.post(check_url, headers=headers, data=data2, verify=False)
    print('本次查验耗时', time.time() - s)
    code_map = {
        '001': '查询成功',
        '009': '查无此票',
        '006': '不一致',
        '007': '验证码失效',
        '008': '验证码错误',
        '002': '超过当日查询次数',
        '003': '验证码请求频繁'
    }

    if res2.json()['key1'] == '001':
        print('查询成功')
        d = parser(res2.json(), data2, province[0])
        json_str = json.dumps({'校验状态': True, '真伪': True, '结果信息': d}, ensure_ascii=False)
        return json_str
    elif res2.json()['key1'] == '002':
        json_str = json.dumps({'校验状态': True, '真伪': True, '结果信息': code_map[res2.json()['key1']]}, ensure_ascii=False)
        return json_str
    elif res2.json()['key1'] in ['009', '006']:
        json_str = json.dumps({'校验状态': True, '真伪': False, '结果信息': code_map[res2.json()['key1']]}, ensure_ascii=False)
        return json_str
    elif res2.json()['key1'] in ['007', '008']:
        raise CaptchaError
    else:
        code_info = code_map.get(res2.json()['key1'], None)
        print('查询失败: ', code_info)
        json_str = json.dumps({'校验状态': False, '真伪': False, '结果信息': code_info}, ensure_ascii=False)
        return json_str


def fplx_title(fpdm):
    if len(fpdm) == 12:
        title = '普通发票'
        if get_fplx(fpdm) == '11':
            title = '普通发票(卷票)'
    elif len(fpdm) == 10:
        title = '专用发票'
    else:
        raise ValueError('暂未支持的发票类型：%s' % fpdm)
    return title


def tract_tax_rate(rate_str):
    rate_str = rate_str.strip()
    if rate_str == '':
        return rate_str
    elif float(rate_str) > 1:
        return f'{rate_str}%'
    else:
        return f'{float(rate_str) * 100}%'


def parser(json_obj, post_data, province):
    k2_l = json_obj['key2'].split('≡')
    fplx = fplx_title(post_data['key1'])
    good_data = []
    good_list = json_obj['key3'].split('≡')
    title = f'{province}增值税{fplx}'
    if fplx == '普通发票':
        for good in good_list:
            infos = good.split('█')
            good_data.append({
                '货物或应税劳务服务名称': infos[0],
                '规格型号': infos[1],
                '单位': infos[2],
                '数量': infos[6],
                '单价': infos[4],
                '金额': infos[5],
                '税率': tract_tax_rate(infos[3]),
                '税额': infos[7],
            })
        data = {
            '查验次数': int(k2_l[0]) + 1,
            '查验时间': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            '标题': title,
            '发票代码': post_data['key1'],
            '发票号码': post_data['key2'],
            '开票日期': k2_l[1],
            '校验码': k2_l[10],
            '机器编号': k2_l[14],
            '购买方名称': k2_l[6],
            '购买方纳税人识别号': k2_l[7],
            '购买方地址电话': k2_l[8],
            '购买方开户行及账号': k2_l[9],
            '商品信息总': good_data,
            '价税合计大写': '',
            '价税合计小写': k2_l[12],
            '销售方名称': k2_l[2],
            '销售方纳税人识别号': k2_l[3],
            '销售方地址电话': k2_l[4],
            '销售方开户行及账号': k2_l[5],
            '备注': json_obj['key4'],
        }
    elif fplx == '专用发票':
        for good in good_list:
            infos = good.split('█')
            good_data.append({
                '货物或应税劳务服务名称': infos[0],
                '规格型号': infos[1],
                '单位': infos[2],
                '数量': infos[3],
                '单价': infos[4],
                '金额': infos[5],
                '税率': tract_tax_rate(infos[6]),
                '税额': infos[7],
            })
        data = {
            '查验次数': int(k2_l[0]) + 1,
            '查验时间': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            '标题': title,
            '发票代码': post_data['key1'],
            '发票号码': post_data['key2'],
            '开票日期': k2_l[1],
            '校验码': k2_l[16],
            '机器编号': k2_l[14],
            '购买方名称': k2_l[2],
            '购买方纳税人识别号': k2_l[3],
            '购买方地址电话': k2_l[4],
            '购买方开户行及账号': k2_l[5],
            '商品信息总': good_data,
            '价税合计大写': '',
            '价税合计小写': k2_l[12],
            '销售方名称': k2_l[6],
            '销售方纳税人识别号': k2_l[7],
            '销售方地址电话': k2_l[8],
            '销售方开户行及账号': k2_l[9],
            '备注': json_obj['key4'],
        }
    elif fplx == '普通发票(卷票)':
        for good in good_list:
            infos = good.split('█')
            good_data.append({
                '项目': infos[0],
                '单价': infos[2],
                '数量': infos[1],
                '金额': infos[3],

            })
        data = {
            '查验次数': int(k2_l[0]) + 1,
            '查验时间': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            '标题': title,
            '发票代码': post_data['key1'],
            '发票号码': post_data['key2'],
            '机打号码': post_data['key2'],
            '开票日期': k2_l[1],
            '校验码': k2_l[10],
            '机器编码': k2_l[6],
            '购买方单位': k2_l[4],
            '购买方税号': k2_l[5],
            '商品信息总': good_data,
            '金额合计': k2_l[8],
            '销售方名称': k2_l[2],
            '销售方税号': k2_l[3],
            '备注': json_obj['key4'],
        }

    else:
        raise ValueError('未知发票类型')
    # pprint(data)
    return data


def invoice_check(params, retry=10):
    fpdm = params['fpdm']
    fphm = params['fphm']
    kprq = params['kprq']
    kjje = params['kjje']
    kpje = params['kpje']
    yzm_user = params['yzm_user']
    yzm_pwd = params['yzm_pwd']
    try:
        return check(fpdm, fphm, kprq, kjje, kpje, yzm_user, yzm_pwd)
    except CaptchaError:
        if retry > 0:
            print('重新获取验证码剩余次数：%s' % (retry-1))
            return invoice_check(params, retry - 1)
        else:
            return json.dumps({'校验状态': False, '真伪': False, '结果信息': '验证码连续识别出错'}, ensure_ascii=False)
    except Exception as ex:
        traceback.print_exc()
        if retry > 0:
            print('异常重试剩余次数：%s' % (retry-1))
            return invoice_check(params, retry - 1)
        else:
            return json.dumps({'校验状态': False, '真伪': False, '结果信息': '识别出现异常: %s' % ex}, ensure_ascii=False)
