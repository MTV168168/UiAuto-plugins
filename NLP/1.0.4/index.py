import jieba.posseg as pg
from jieba import analyse


def lac(params):
    try:
        string = params['string']
        words = pg.cut(string)
        result = list()
        for word, flag in words:
            temp = dict()
            temp['index'] = string.index(word)
            temp['pos'] = flag
            temp["text"] = word
            result.append(temp)
        return result
    except Exception as e:
        raise e


def extract(params):
    try:
        string = params['string']
        extracts = analyse.extract_tags
        textranks = analyse.textrank
        words = extracts(string)
        words = textranks(string)
        result = list()
        for w in words:
            for word in lac(params):
                if word['text'] == w:
                    result.append(word)
                    break
        return result
    except Exception as e:
        raise e


if __name__ == '__main__':
    p = {
        "string": "小明硕士毕业于中国科学院计算所，后在日本京都大学深造"
    }

    print(type(extract(p)))