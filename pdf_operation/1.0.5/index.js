const _ = require("lodash");
const fse = require('fs-extra');
const fs = require('fs');
const path = require("path");
const PDFDocument = require('pdfkit');
const sizeOf = require('image-size');
const PDFMerger = require('pdf-merger-js');

exports.mergeFn = function (params) {
    return new Promise((resolve, reject) => {
        // 文件路径列表
        let filePathLs = [];
        // 文件夹路径列表
        let directoryPathLs = [];
        if (params.file_or_directory == 'directory_path') {
            directoryPathLs = params.directory_path;
            _.each(directoryPathLs, directoryPathItem => {
                readdirSyncFn(directoryPathItem, filePathLs)
            })
            _.remove(filePathLs, filePathItem => {
                return filePathItem == '.DS_Store';
            })
        } else if (params.file_or_directory == 'file_path') {
            _.each(params.file_path, item => {
                filePathLs.push(item);
            })
        }
        temp = params.uiauto_config.log_file.split('\\')
        var project_name = temp[temp.length-2]
        // 保存路径
        let savePath = path.normalize(`${params.uiauto_config.projects_dir}/${project_name}/file`);
        if (!fs.existsSync(savePath)) {
            fse.ensureDirSync(savePath);
        }


        // 保存文件名
        let saveName = params.saveName.split('.')[1] == 'pdf' ? params.saveName : params.saveName + '.pdf';

        if (params.fileFormat == 'pdf') {
            _.remove(filePathLs, filePathItem => {
                return extname(filePathItem) != 'pdf';
            })
            if (filePathLs.length) {
                let merger = new PDFMerger();
                _.each(filePathLs, filePathItem => {
                    merger.add(filePathItem);
                })
                merger.save(path.normalize(`${savePath}/${saveName}`));
                resolve(path.normalize(`${savePath}/${saveName}`))
            } else {
                reject('无需拼接的文件');
            }

        } else {
            _.remove(filePathLs, filePathItem => {
                return !_.includes(['png', 'jpeg', 'jpg'], extname(filePathItem));
            })
            if (filePathLs.length) {
                let doc = new PDFDocument({
                    size: [595, 842],
                    margins: {
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0
                    }
                })
                doc.pipe(fs.createWriteStream(path.normalize(`${savePath}/${saveName}`)));

                let pagePicSize = parseInt(2377 / sizeOf(filePathLs[0]).height);
                filePathLs = _.chunk(filePathLs, pagePicSize);

                _.each(filePathLs, (groupItem, idx) => {
                    _.each(groupItem, filePathItem => {
                        doc.image(filePathItem, {
                            fit: [595, 842],
                            align: 'center'
                        });
                    })
                    if (idx < filePathLs.length - 1) {
                        doc.addPage({
                            size: [595, 842],
                            margins: {
                                top: 0,
                                bottom: 0,
                                left: 0,
                                right: 0
                            }
                        })
                    }
                })
                doc.end();

                resolve(path.normalize(`${savePath}/${saveName}`));

            } else {
                reject('无需拼接的文件');
            }
        }
    })
}

function readdirSyncFn(directoryPathItem, filePathLs) {
    if (fs.existsSync(directoryPathItem)) {
        files = fs.readdirSync(directoryPathItem);
        files.forEach(function (file, index) {
            var curPath = path.normalize(`${directoryPathItem}/${file}`);
            if (fs.statSync(curPath).isDirectory()) { // recurse
                readdirSyncFn(curPath, filePathLs);
            } else { // delete file
                filePathLs.push(curPath);
            }
        });
    }
}

// 获取文件后缀名
function extname(filename) {
    if (!filename || typeof filename != 'string') {
        return false
    };
    let a = filename.split('').reverse().join('');
    let b = a.substring(0, a.search(/\./)).split('').reverse().join('');
    return b
};
