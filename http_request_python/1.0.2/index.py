import requests
import json

def httpRequestFn(params):
    try:
        if params['httpRequestLink'] is None or params['httpRequestLink'] == '':
            raise Exception('缺少参数：访问链接')
        else:
            res = None
            if params['httpRequestMethod'] == 'post':
                res = requests.post(params['httpRequestLink'], headers=params['httpRequestHeader'], data=params['httpRequestParams'])
            elif params['httpRequestMethod'] == 'get':
                res = requests.get(params['httpRequestLink'], headers=params['httpRequestHeader'], data=params['httpRequestParams'])
            else:
                pass

            if res is not None:
                if res.status_code == 200:
                    try:
                        return json.loads(res.text)
                    except Exception:
                        return res.text
                else:
                    raise Exception(res.text)
            else:
                return res
    except Exception as e:
        raise e
