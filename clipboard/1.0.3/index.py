import struct
import win32clipboard
import win32con
from io import BytesIO
from PIL import Image
import os


def setText(params):
    try:
        text = str(params['text'])
        win32clipboard.OpenClipboard()
        win32clipboard.EmptyClipboard()
        win32clipboard.SetClipboardData(win32con.CF_UNICODETEXT, str(text))
    except Exception as e:
        raise e
    finally:
        win32clipboard.CloseClipboard()
    return text


def getText(params):
    try:
        win32clipboard.OpenClipboard()
        text = win32clipboard.GetClipboardData(win32con.CF_UNICODETEXT)
    except Exception as e:
        raise e
    finally:
        win32clipboard.CloseClipboard()
    return text


def setImage(params):
    try:
        path = params['path']
        filepath, filename = os.path.split(path)
        win32clipboard.OpenClipboard()
        win32clipboard.EmptyClipboard()
        img = Image.open(path)
        output = BytesIO()
        img.convert('RGB').save(output, "BMP")
        data = output.getvalue()[14:]
        output.close()
        win32clipboard.SetClipboardData(win32con.CF_DIB, data)
        return filename
    except Exception as e:
        raise e
    finally:
        win32clipboard.CloseClipboard()


def getImage(params):
    try:
        path = params['path']
        filename = params['name']
        win32clipboard.OpenClipboard()
        if win32clipboard.IsClipboardFormatAvailable(win32clipboard.CF_DIB):
            sBitmap = win32clipboard.GetClipboardData(win32clipboard.CF_DIB)
            # 构建 BMP 文件头 [ BM + (int)Bitmap数据长度 + (int)Reserved + (int)OffBits ]
            sBmpHdr = struct.pack('<HIII', 0x4d42, len(sBitmap), 0, 54)
            with open(path + "\\" + filename, 'wb') as sFile:
                sFile.write(sBmpHdr)
                sFile.write(sBitmap)
    except Exception as e:
        raise e
    finally:
        win32clipboard.CloseClipboard()

