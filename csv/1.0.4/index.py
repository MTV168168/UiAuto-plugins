import csv


def openCSV(params):
    try:
        path = params["path"]
        reader = None
        with open(path, 'rb') as objFile:
            reader = csv.reader(objFile)
        result = list(reader)
        # for index, r in enumerate(result):
        #     # if type(r) == 'int':
        #     #     result[index] = str(r)
        #     print(index, r)
        return result
    except Exception as e:
        raise e


def saveCSV(params):
    try:
        data = params["data"]
        path = params["path"]
        if params['type'] == 'append':
            mode = 'a'
        else:
            mode = 'w'
        with open(path, mode, newline='', encoding='utf-8-sig') as objFile:
            writer = csv.writer(objFile)
            for row in data:
                for index, d in enumerate(row):
                    if isinstance(d, str) and d.isdigit():
                        # print('str', d)
                        row[index] = str(d) + '\t'
                    # print(index, d)
                writer.writerow(row)
    except Exception as e:
        raise e
